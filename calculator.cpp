#include <iostream>
#include <windows.h>
using namespace std;

int main()
{

    int num1;
    int num2;
    int result;
    int operation;
    string wanna_stop;
    bool end;

    cout << "Hi, iam your calculator" << endl;
    cout << "\n"
         << endl;
    Sleep(1000);
    end = false;
    while (not end)

    {
        cout << "What operation do you want to prove?? \nWrite the operation number: " << endl;
        cout << "1) Addition" << endl;       //+
        cout << "2) Multiplication" << endl; //x
        cout << "3) Subtraction" << endl;    //-
        cout << "4) Division" << endl;       //÷
        cin >> operation;

        if (operation >= 5)
        {
            cout << "Try againt!" << endl;
        }

        else
        {
            cout << "write first number: " << endl;
            cin >> num1;
            cout << "write second number: " << endl;
            cin >> num2;

            switch (operation)
            {
            case 1:
                result = num1 + num2;
                break;
            case 2:
                result = num1 * num2;
                break;
            case 3:
                result = num1 - num2;
                break;
            case 4:
                if (num2 == 0)
                {
                    cout << "Number cannot be divided by zero, please try again" << endl;
                    result = 0;
                }
                else
                {
                    result = num1 / num2;
                }
                break;
            default:
                cout << "You entered the wrong option, please try again" << endl;
                result = 53;
            }

            cout << "result: " << result << endl;
            Sleep(1500);
        }

        cout << "You wanna continue: YES nebo NO" << endl;
        cin >> wanna_stop;
        if ((wanna_stop == "NO") || (wanna_stop == "no") || (wanna_stop == "No"))
        {
            end = true;
        }
    }
    cout << "Exit in..." << endl;
    Sleep(1000);
    cout << "3..." << endl;
    Sleep(1000);
    cout << "2..." << endl;
    Sleep(1000);
    cout << "1..." << endl;
    Sleep(1000);

    return 0;
}